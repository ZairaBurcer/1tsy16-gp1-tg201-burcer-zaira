﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to Recettear!\n");

            Player player = new Player("Recet", 100);
            Shop shop = new Shop();

            string input = "";
            while (input != "n")
            {
                // Step 1: Setup Shop.
                shop.PrintItems();
                Console.WriteLine("\nSetup Shop");
                string shopInput = "";
                while (true)
                {
                    Console.Write("Would you like to add more items (y)/(n)?");
                    shopInput = Console.ReadLine();
                    if (shopInput == "n")
                    {
                        break;
                    }

                    /// QUIZ ITEM
                    /// Print all available items in the game
                    /// You can get them from ItemDatabase.GetItem(int id)
                    /// There are only 6 valid items, w/ id numbers from 1-6
                    /// Hint: For loop
                    // Print all items available
                    for (int i = 1; i <= 6; i++)
                    {
                        ItemDatabase.GetItem(i).Print();
                    }
                    

                    // Ask the user w/c item number he would like to add
                    Console.Write("Please enter the item number you would like to add to your store:");
                    int itemId = Int32.Parse(Console.ReadLine());

                    /// QUIZ ITEM
                    /// Get the item from the database by ItemDatabase.GetItem(int id)
                    Item item = ItemDatabase.GetItem(itemId);
                    shop.ShopInventory.Add(item);
                   

                    /// QUIZ ITEM
                    /// Add the item to the store's list of available items
                    
                    Console.WriteLine();
                }

                shop.PrintItems();

                Console.WriteLine();
                Console.WriteLine("Customers have entered the store!");
                // Step 2: Customers come in
                List<NPC> npcs = new List<NPC>();
                int npcCount = random.Next(1, 6);
                for (int i = 0; i < npcCount; i++)
                {
                    int RandomNPC = random.Next(1, 7);
                    NPC npcsRandom = NPCDatabase.GetNpc(RandomNPC);
                    npcs.Add(npcsRandom);

                    /// QUIZ ITEM
                    /// Randomize an NPC ID from 1-6
                    /// and add it to the npcs list
                    /// You can get an NPC by calling
                    /// NPCDatabase.GetNPC(int id)
                }

                // Step 3: Resolve orders
                foreach (NPC npc in npcs)
                {
                    Console.WriteLine("A customer is shopping for {0}", npc.ItemNameToBuy);
                    shop.Transact(npc);
                    Console.WriteLine();
                }

                // Step 4: End of the day, display player's earnings
                /// QUIZ ITEM
                /// Compute the player's money earned
                /// Hint: Store the starting money in a variable before you setup shop
                Console.WriteLine("The shop has closed, you have earned ");
                Console.Write("Continue playing (y)/(n)?: ");
                input = Console.ReadLine();
            }
        }
    }
}
