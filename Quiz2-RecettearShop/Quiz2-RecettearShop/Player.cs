﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Player
    {
        public string Name { get; private set; }

        // Backing variable for public property Pix
        private int pix;
        public int Pix
        {
            get { return pix; }
            set
            {
                if (value < 0)
                    pix = 0;
                else
                    pix = value;
            }

        }

        public Player(string name, int startingPix)
        {
            Name = name;
            Pix = startingPix;
        }
    }
}
