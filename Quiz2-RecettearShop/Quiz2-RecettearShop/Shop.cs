﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class Shop
    {
        /// QUIZ ITEM
        /// Declare a list of items here
        /// </summary>
        public List<Item> ShopInventory = new List<Item>();
        private Player player;
        
        /// QUIZ ITEM
        /// Pass in the player as a parameter and assign it to the private player; variable
        public Shop()
        {
            /// QUIZ ITEM
            /// Initialize the list here
        }

        /// QUIZ ITEM
        /// Implement the transact function, this is when an NPC is about to buy an item
        /// Search your available items for a match, if there's a match
        /// Remove that item from your list, add the money to the player
        /// If there isn't a match, just print "Customer wasn't able to buy anything"
        /// Hint: Linear Search
        
        public void Transact(NPC npc)
        {
            for (int i = 0; i < ShopInventory.Count; i++)
            {
                if (npc.ItemNameToBuy == ShopInventory[i].Name)
                {
                    Console.WriteLine("Customer purchased " + ShopInventory[i].Name + " worth " + ShopInventory[i].BasePrice);
                    player.Pix += ShopInventory[i].BasePrice;
                    ShopInventory.Remove(ShopInventory[i]);
                }
                else
                    Console.WriteLine("Customer wasn't able to purchase any item.");
            }
        }

        /// QUIZ ITEM
        /// Implement the print function. Loop through all your items from your list and
        /// print them one by one by calling the item.Print() function.
        /// If there are no items, you should print "-Empty-"
        public void PrintItems()
        {
            Console.WriteLine("\nYour current shop items:");
            if (ShopInventory.Count == 0)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                foreach (Item item in ShopInventory)
                {
                    item.Print();
                }
            }
          
        }
    }
}
