﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    class NPC
    {
        public string ItemNameToBuy { get; set; }

        public NPC(string itemName)
        {
            ItemNameToBuy = itemName;
        }
    }
}
