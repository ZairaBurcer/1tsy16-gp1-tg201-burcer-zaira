﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    static class ItemDatabase
    {
        public static Item GetItem(int id)
        {
            if (id == 1) return new Item("Walnut Bread", 100);
            else if (id == 2) return new Item("Shortsword", 300);
            else if (id == 3) return new Item("Longsword", 1200);
            else if (id == 4) return new Item("Armor", 800);
            else if (id == 5) return new Item("Shortcake", 300);
            else if (id == 6) return new Item("Candy", 120);
            else return null;
        }
    }
}
