﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Player
    {
        public string Name { get; private set; }

        // Constructor
        public Player()
        {
            Name = "Default";
            className = "Default";
            accuracy = 0;
            hitPoints = 0;
            maxHitPoints = 0;
            mp = 0;
            maxMP = 0;
            gold = 0;
            expPoints = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);
        }

        public void CharacterRace()
        {
            Console.WriteLine("Please select a character class race number..");
            Console.WriteLine("1)Dwarf 2)Human 3)Elf 4)Halfling");

            int characterRace = 1;

            characterRace = Convert.ToInt32(Console.ReadLine());

            switch (characterRace)
            {
                case 1: //Dwarf
                    classRace = "Dwarf";
                    accuracy += 1;
                    hitPoints += 5;
                    break;
                case 2: //Human
                    classRace = "Human";
                    hitPoints += 5;
                    Armor += 1;
                    break;
                case 3: // elf
                    classRace = "Elf";
                    accuracy += 5;
                    hitPoints += 1;
                    break;
                default: // halfling
                    classRace = "Halfling";
                    accuracy += 1;
                    hitPoints += 1;
                    maxHitPoints += 2;
                    break;
            }

        }

        public void CreateClass()
        {
            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1)Fighter 2)Wizard 3)Cleric 4)Thief : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    accuracy = 10;
                    hitPoints = 20;
                    maxHitPoints = 20;
                    mp = 10;
                    maxMP = 10;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 4;
                    weapon = new Weapon("Long Sword", 1, 8);
                    spell = new Spell("Fireball", 1, 8, 5);
                    
                    break;
                case 2: //Wizard
                    className = "Wizard";
                    accuracy = 5;
                    hitPoints = 10;
                    mp = 30;
                    maxMP = 30;
                    maxHitPoints = 10;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 1;
                    weapon = new Weapon("Staff", 1, 4);
                    spell = new Spell("Meteorshower", 1, 8, 5);
                    break;
                case 3:
                    className = "Cleric";
                    accuracy = 8;
                    hitPoints = 15;
                    maxHitPoints = 15;
                    mp = 20;
                    maxMP = 20;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 3;
                    weapon = new Weapon("Flail", 1, 6);
                    spell = new Spell("Holy", 1, 8, 5);
                    break;
                default: //
                    className = "Thief";
                    accuracy = 7;
                    hitPoints = 12;
                    maxHitPoints = 12;
                    mp = 15;
                    maxMP = 15;
                    expPoints = 0;  
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 2;
                    weapon = new Weapon("Dagger", 1, 6);
                    spell = new Spell("Steal", 1, 8, 5);
                    break;
            }

        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            Console.Write("1) Attack, 2) Run, 3)Spell");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        }
                        else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
                case 3:
                    Console.WriteLine("You attack" + monsterTarget.Name + "with" + spell.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(spell.DamageRange);
                        int totalDamage = damage;
                        if (mp <= MagicPointsRequired)
                        {
                            Console.WriteLine("You don't have enough Mana");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                            mp -= MagicPointsRequired;
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss");
                        mp -= MagicPointsRequired;
                    }
                    break;
            }
                    return false;
         }
        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly
                accuracy += RandomHelper.Random(1, 3);
                maxHitPoints += RandomHelper.Random(2, 6);
                Armor += RandomHelper.Random(1, 2);
                maxMP += RandomHelper.Random(2, 6);

                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;

                mp = maxMP;

                switch (className)
                {
                    case "Fighter" :
                        accuracy += RandomHelper.Random(1, 3);
                        maxHitPoints += RandomHelper.Random(1, 6);
                        Armor += RandomHelper.Random(1, 2);
                        maxMP += RandomHelper.Random(2, 6);
                        break;
                    case "Wizard":
                        accuracy += RandomHelper.Random(1, 3);
                        maxHitPoints += RandomHelper.Random(1, 4);
                        Armor += RandomHelper.Random(1, 2);
                        maxMP += RandomHelper.Random(2, 6);
                        break;
                    case "Cleric":
                        accuracy += RandomHelper.Random(1, 3);
                        maxHitPoints += RandomHelper.Random(2, 6);
                        Armor += RandomHelper.Random(1, 2);
                        maxMP += RandomHelper.Random(2, 6);
                        break;
                    case "Thief":
                        accuracy += RandomHelper.Random(1, 6);
                        maxHitPoints += RandomHelper.Random(2, 6);
                        Armor += RandomHelper.Random(1, 2);
                        maxMP += RandomHelper.Random(2, 6);
                        break;
                }
            }
        }

        public void Rest()
        {
            Console.WriteLine("Resting...");

            hitPoints = maxHitPoints;

            // TODO: Modify the function so that random enemy enounters
            // are possible when resting

        }

        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Class            = " + classRace + " " + className);
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("Mp               = " + mp.ToString());
            Console.WriteLine("MaxMP            = " + maxMP.ToString());
            Console.WriteLine("Gold             = " + gold.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());

            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }
        
        public void Victory(int xp, int reward)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            Console.WriteLine("You win " + reward.ToString() + " Gold!");
            gold += reward;
            expPoints += xp;
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }
        
        private string className;
        private string classRace;
        private int hitPoints;
        private int maxHitPoints;
        private int gold;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int accuracy;
        private int mp;
        private int MagicPointsRequired;
        private int maxMP;
        private Weapon weapon;
        private Spell spell;
    }
}
