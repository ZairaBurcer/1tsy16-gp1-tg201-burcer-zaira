﻿using UnityEngine;
using System.Collections;

public class cameraMovement : MonoBehaviour
{

    public GameObject MainCamera;

    public Vector3 offset;

    // Use this for initialization
    void Start()
    {

        offset = transform.position - MainCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = MainCamera.transform.position + offset;
    }
}
