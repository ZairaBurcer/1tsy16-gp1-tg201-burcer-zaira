﻿using UnityEngine;
using System.Collections;

public class PlayerBullets : MonoBehaviour {

    public GameObject PlayerBullet;
    private float Spawntime = 1f;
    public float SpawnRate;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("BulletSpawner", Spawntime, SpawnRate);
    }

    void BulletSpawner()
    {
        Instantiate(PlayerBullet, this.transform.position, Quaternion.identity);

    }
}
