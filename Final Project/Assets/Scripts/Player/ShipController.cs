﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{
    public GameObject Player;
    public GameObject[] Life;
    private int HPlife = 3;
    public LineRenderer line;
    public float speed;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Life = GameObject.FindGameObjectsWithTag("HP");
        line.GetComponent<LineRenderer>();
    }

    void Update()
    {
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 distance = mouse - this.transform.position;

        float mag = distance.magnitude;
        if (mag >= 1)
        {
            transform.Translate(distance.normalized * Time.deltaTime * speed);
        }
        line.SetPosition(0, mouse);
        line.SetPosition(1, this.transform.position);

        float boundx = Mathf.Clamp(transform.position.x, -40, 40);
        float boundy = Mathf.Clamp(transform.position.y, -20, 20);
        transform.position = new Vector3(boundx, boundy, 0.0f);

        if (HPlife <= 0)
        {
            Destroy(gameObject);
        }

    }
    void OnTriggerEnter2D(Collider2D targetPlayer)
    {
        if (targetPlayer.tag == "AI" && HPlife > 0)
        {
            //print(HPlife);
            Life[HPlife - 1].SetActive(false);
            HPlife--;

        }
    }
}
