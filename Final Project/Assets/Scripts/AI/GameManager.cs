﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public GameObject EnemyShip;
    public GameObject PirateShip2;
    public GameObject AI;
    public GameObject AI2;
    float maxSpawnRateInSeconds = 2f;

    void Start()
    {
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);

        InvokeRepeating("IncreaseSpawnRate", 0f, 30);
    }

    void SpawnEnemy()
    {
        GameObject anEnemy = (GameObject)Instantiate(AI);
        anEnemy.transform.position = Camera.main.ViewportToWorldPoint(new Vector2((Random.Range(0f, 1.5f)), (Random.Range(0f, 1.5f))));
        nextEnemySpawn();

        int roll = Random.Range(1, 4);
        if (roll == 1)
        {
            Instantiate(EnemyShip, Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1.5f), Random.Range(0f, 1.5f))), Quaternion.identity);
        }
        if (roll == 2)
        {
            Instantiate(AI2, Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1.5f), Random.Range(0f, 1.5f))), Quaternion.identity);
        }
        if (roll == 3)
        {
            Instantiate(PirateShip2, Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1.5f), Random.Range(0f, 1.5f))), Quaternion.identity);
        }

    }

    void nextEnemySpawn()
    {
        float spawnInSeconds;

        if (maxSpawnRateInSeconds > 1f)
        {
            spawnInSeconds = Random.Range(1f, maxSpawnRateInSeconds);
        }
        else
        {
            spawnInSeconds = 1f;
        }

        Invoke("SpawnEnemy", spawnInSeconds);
    }
    void IncreaseSpawnRate()
    {
        if (maxSpawnRateInSeconds > 1f)
        {
            maxSpawnRateInSeconds--;
        }
        if (maxSpawnRateInSeconds == 1f)
        {
            CancelInvoke("IncreaseSpawnRate");
        }
    }
}
