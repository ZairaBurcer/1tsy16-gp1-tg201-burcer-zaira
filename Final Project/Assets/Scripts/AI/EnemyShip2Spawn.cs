﻿using UnityEngine;
using System.Collections;

public class EnemyShip2Spawn : MonoBehaviour
{
    private int HP = 7;

    void OnTriggerEnter2D(Collider2D PlayerBullet)
    {
        if (PlayerBullet.tag == "AI")
        {
            HP--;
            Destroy(PlayerBullet.gameObject);

            if (HP < 0)
            {
                Destroy(gameObject, 1);
            }
        }
    }
}


