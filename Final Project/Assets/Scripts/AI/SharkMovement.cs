﻿using UnityEngine;
using System.Collections;

public class SharkMovement : MonoBehaviour
{
    public float speed;
    public GameObject Player;
    private Vector3 distance;
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        distance = Player.transform.position - transform.position;
        

    }
    void Update()
    {
        distance.Normalize();
        transform.position += (distance * speed * Time.deltaTime);
    }
} 
