﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            int NumberOfSpy = 0;

            if (totalPlayers >= 5 && totalPlayers <= 6)
            {
                NumberOfSpy = 2;
            }
            else if (totalPlayers >= 7 && totalPlayers <= 8)
            {
                NumberOfSpy = 3;
            }
            else if (totalPlayers >= 9 && totalPlayers <= 10)
            {
                NumberOfSpy = 4;
            }
            return NumberOfSpy;
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies
        }

        static int EvaluateRound(string[] players)
        {
            int NoOfSuccess = 0;
            int NoOfFail = 0;

            for(int i =0; i<players.Length; i++)
            {
                string input = " ";
                if((input != "F") && (input != "S"))
                {
                    Console.WriteLine("Player "+i+" " +players[i] + " (S/F)");
                    input = Console.ReadLine();
                }
                if(input.Equals("S"))
                {
                    NoOfSuccess++;
                }
                else if(input.Equals("F"))
                {
                    NoOfFail++;
                }
            }
            if(NoOfSuccess>1)
            {
                return 0;
            }
            else
            {
                return 1;
            }
            // Return 0 if resistance wins
             //Return 1 if spies win
        }

        static void PrintPlayers(string[] players)
        {
            for (int i = 0; i < players.Length; i++)
            {
                Console.Write("Player");
                Console.WriteLine(i + 1 + ":" + players[i]);
            }
        }

        static void AssignRoles(string[] players, int Number)
        {
            int MaxNoOfSpies = GetNumSpies(players.Length);
            int CurrentNoOfSpies = 0;

            for (int x = 0; x < players.Length; x++)
            {
                int role = random.Next(0, 2);
                if (role == 0)
                {
                    players[x] = "Resistance";
                }
                else if (role == 1)
                {
                    players[x] = "Spy";
                    CurrentNoOfSpies++;
                    if (CurrentNoOfSpies > MaxNoOfSpies)
                    {
                        players[x] = "Resistance";
                    }
                }
            }
            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            PrintPlayers(players);
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            int NumberOfPlayers = 0;

            if (totalPlayers >= 5 && totalPlayers <= 6)
            {
                NumberOfPlayers = 3;
            }
            else if (totalPlayers >= 7 && totalPlayers <= 8)
            {
                NumberOfPlayers = 4;
            }
            else if (totalPlayers >= 9 && totalPlayers <= 10)
            {
                NumberOfPlayers = 5;
            }
            return NumberOfPlayers;
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            int playersNum = 0;

            /////////////////////
            // QUIZ ITEM: Loop until the player enters a valid number
            /////////////////////
            Console.Write("Enter number of players (5-10)");
            playersNum = Convert.ToInt32(Console.ReadLine());
            while ((playersNum <= 4) || (playersNum >= 11))
            {
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            }
            // END LOOP

            //// Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            Console.ReadKey();
            // Assign roles to the players
            AssignRoles(players, playersNum);
            Console.Read();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());

                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();

        }
    }
}