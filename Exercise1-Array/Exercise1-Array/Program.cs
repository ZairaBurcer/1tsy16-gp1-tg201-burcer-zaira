﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LinearSearch
{
    class Program
    {
        static bool SearchArray(string[] Items, string itemToSearch)
        {
            for (int i = 0; i < Items.Length; i++)
            {
                if (Items[i].Equals(itemToSearch))
                {
                    return true;
                }
            }
            return false;
        }
        static int Number(string[] item, string itemNumber)
        {
            int count = 0;
            for (int y = 0; y < item.Length; y++)
            {
                if (itemNumber.Equals(item[y]))
                {
                    count++;
                }
            }
            return count;
        }
        static void Main(string[] args)
        {
            string[] Items = { "Health Potion", "Mana Potion", "Mana Potion", "Mana Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe", "Dreadwyrm Staff" };
            Console.WriteLine("Input Item: ");
            string itemToSearch = (Console.ReadLine());
            if (SearchArray(Items, itemToSearch).Equals(true))
            {
                Console.WriteLine("Item Found! Number of items: " + Number(Items, itemToSearch));
            }
            else
            { Console.WriteLine("Item not found"); }
            Console.ReadKey();

        }
    }
}