﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public Assassin()
        {
            Type = "Assassin";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Warrior")
            {
                Console.WriteLine("You win!");
                return FightResult.Win;
            }
            else if (opponentCard.Type == "Mage")
            {
                Console.WriteLine("You lose!");
                return FightResult.Lose;
            }
            else
            {
                return FightResult.Draw;
            }
        }
    }
}
