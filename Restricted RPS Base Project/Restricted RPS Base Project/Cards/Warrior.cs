﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior()
        {
            Type = "Warrior";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Warrior")
            {
                return FightResult.Draw;
            }
            else if (opponentCard.Type == "Mage")
            {
                Console.WriteLine("You win!");
                return FightResult.Win;
            }
            else
            {
                Console.WriteLine("You lose!");
                return FightResult.Lose;
            }

        }
    }
}
