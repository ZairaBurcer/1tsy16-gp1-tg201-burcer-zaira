﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Console.WriteLine("Hand:\n");
            DisplayCards();
            Console.WriteLine("\nPress 1 to challenge " + opponent.Name);
            Console.WriteLine("Press 2 Discard");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice.Equals('1'))
            {
                PlayCard();
                return;
            }
            else if (choice.Equals('2'))
            {
                Discard();
                return;
            }

        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            throw new NotImplementedException();
        }
    }
}
