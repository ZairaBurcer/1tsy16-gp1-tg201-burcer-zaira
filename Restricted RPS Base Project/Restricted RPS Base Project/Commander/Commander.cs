﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                if (cards.Count >= 2)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            Card card = null;
            int meshmesh = RandomHelper.Range(3);
            int drawCard = Convert.ToInt32(Console.ReadLine());

            switch (meshmesh)
            {
                case 1:
                    card = new Warrior();
                    break;
                case 2:
                    card = new Assassin();
                    break;
                case 3:
                    card = new Mage();
                    break;
            }
            cards.Add(card);
            return null;

        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            Console.WriteLine("Player sacrificed 2 units to summon a new hero");
            if (!CanDiscard)
            {
                throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE
            }
            int cardRemove;
            for (int z = 0; z < 2; z++)
            {
                cards.RemoveAt(RandomHelper.Range(cards.Count));
                cardRemove = z;
            }
            return null;
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            for (int x = 0; x < cards.Count; x++)
            {
                Console.WriteLine("\t" +cards[x].Type);
            }

        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            if (cards.Count > 0)
            {
                Points -= cards.Count;
            }
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;
            }
            else if (result == FightResult.Lose)
            {
            }
            else if (result == FightResult.Draw)
            {
                Console.WriteLine("Draw");
            }
        }
    }
}
