﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            if (!CanDiscard)
            {
                Fight(opponent);
            }
            else
            {
                if (RandomHelper.Chance(0.7f))
                {
                    Fight(opponent);
                }
                else
                {
                    Discard();
                }

            }

        }

        public override Card PlayCard()
        {
            Card card = Cards[RandomHelper.Range(Cards.Count)];
            Cards.Remove(card);
            return card;
        }
    }
}
