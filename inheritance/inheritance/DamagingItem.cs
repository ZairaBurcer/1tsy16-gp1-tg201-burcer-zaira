﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    public class DamagingItem : Item
    {
        public int Damage { get; private set; }

        public DamagingItem() : base()
        {
            Damage = 0;
        }

        public DamagingItem(string name, int cost, int damage) : base(cost, name)
        {
            Damage = damage;
        }

        public override void UseItem()
        {
            base.UseItem();

            Console.WriteLine("Your opponent took {0} damage!", Damage);
        }

    }
}
