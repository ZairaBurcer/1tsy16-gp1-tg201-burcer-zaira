﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    public class ConsumableItem : Item
    {
        public int HPAdd { get; private set; }
        public int SPAdd { get; private set; }

        public ConsumableItem() : base()
        {
            HPAdd = 0;
            SPAdd = 0;
        }

        public ConsumableItem(string name, int cost, int hpadd, int spadd) : base(cost, name)
        {
            HPAdd = hpadd;
            SPAdd = spadd;
        }

        public override void UseItem()
        {
            base.UseItem();
            if (HPAdd > 0)
                Console.WriteLine("You recovered {0} HP!", HPAdd);
            if (SPAdd > 0)
                Console.WriteLine("You recovered {0} SP!", SPAdd);
        }
    }
}
