﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    class Program
    {
        static void Main(string[] args)
        {

            Inventory inventory = new Inventory();

            while (true)
            {
                string input;
                Console.Write("Would you like to add more items (y)/(n)?");
                input = Console.ReadLine();
                if (input == "n")
                {
                    break;
                }

                for (int x = 1; x < 4; x++)
                {
                    ItemDatabase.GetItem(x).Print();
                }

                Console.Write("Please enter the item number you would like to add to your bag: ");
                int itemId = Int32.Parse(Console.ReadLine());
                Item itemAdd = ItemDatabase.GetItem(itemId);
                inventory.Items.Add(itemAdd);
                Console.WriteLine();
            }

            inventory.UseAll();
            Console.ReadKey();

        }
    }
}
