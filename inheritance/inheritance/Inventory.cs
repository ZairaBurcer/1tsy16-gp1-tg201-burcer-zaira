﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    public class Inventory
    {
        public List<Item> Items;

        public Inventory()
        {
            Items = new List<Item>();
        }

        public void GetItem(Item item)
        {
            Items.Add(item);
        }

        public void UseAll()
        {
            foreach (Item item in Items)
            {
                item.UseItem();
            }
        }
    }
}
