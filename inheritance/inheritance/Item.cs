﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    public class Item
    {
        public string Name { get; protected set; }
        public int Cost { get; protected set; }

        public Item()
        {
            Name = "Default";
            Cost = 0;
        }

        public Item(int cost, string name)
        {
            Name = name;
            Cost = cost;
        }

        public virtual void UseItem()
        {
            Console.WriteLine("You used {0}.", Name);
        }

        public void Print()
        {
            Console.WriteLine("Name: {0}\t\tPrice: {1}", Name, Cost);

        }

    }
}
