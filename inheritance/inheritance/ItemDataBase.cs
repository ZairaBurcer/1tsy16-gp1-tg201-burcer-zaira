﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    class ItemDatabase
    {
        public static Item GetItem(int id)
        {
            if (id == 1) return new ConsumableItem("Super Crock Pot", 350, 50, 0);
            else if (id == 2) return new BuffingItem("White Gauntlet", 250, 100, "Defense");
            else if (id == 3) return new DamagingItem("Master's Dagger", 120, 30);
            else return null;
        }
    }
}
