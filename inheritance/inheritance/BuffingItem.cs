﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    public class BuffingItem : Item
    {
        public int BuffValue { get; private set; }
        public string BuffStat { get; private set; }

        public BuffingItem() : base()
        {
            BuffValue = 0;
            BuffStat = "Default";
        }

        public BuffingItem(string name, int cost, int buffvalue, string buffstat) : base(cost, name)
        {
            BuffValue = buffvalue;
            BuffStat = buffstat;
        }

        public override void UseItem()
        {
            base.UseItem();

            Console.WriteLine("Your {0} has increased by {1}", BuffStat, BuffValue);
        }

    }
}
