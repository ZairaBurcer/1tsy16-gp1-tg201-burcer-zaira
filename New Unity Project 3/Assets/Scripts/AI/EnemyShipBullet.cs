﻿using UnityEngine;
using System.Collections;

public class EnemyShipBullet : MonoBehaviour {

    public GameObject Player;
    public float speed;
    Vector3 lastPosition;
	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        lastPosition = Player.transform.position;
        Destroy(gameObject, 2);
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 position = lastPosition - transform.position;
        position.Normalize();
        transform.Translate(position * speed * Time.deltaTime);
	}
}
