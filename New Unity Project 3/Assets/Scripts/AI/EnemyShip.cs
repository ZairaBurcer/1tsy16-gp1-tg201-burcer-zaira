﻿using UnityEngine;
using System.Collections;

public class EnemyShip : MonoBehaviour {
    public float speed;
    public float distance;
    private float Shotfire;
    private float fireRate = 0.5f;
    public GameObject Enemy;
    public GameObject Player;
    public GameObject AIBullet;


    void Start()
    {
        Enemy = GameObject.FindGameObjectWithTag("AI");
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        float spaceBetween = Vector3.Distance(transform.position, Player.transform.position);
        if (spaceBetween > distance)
        {
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, speed * Time.deltaTime);
        }

        if( Time.time > Shotfire)
        {
            Shotfire = Time.time + fireRate;
            AIBullet.GetComponent<EnemyShipBullet>().Player = Player;
            Instantiate(AIBullet, this.transform.position, Quaternion.identity);
        }


    }
}
