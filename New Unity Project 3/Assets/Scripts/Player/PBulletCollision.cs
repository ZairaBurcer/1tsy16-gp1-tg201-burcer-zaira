﻿using UnityEngine;
using System.Collections;

public class PBulletCollision : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D targerAI)
    {
        if(targerAI.gameObject.tag == "AI")
        {
            Destroy(targerAI.gameObject);
            Destroy(this.gameObject);
        }
    }
}
