﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{
    public LineRenderer line;
    public float speed;

    void start()
    {
        line.GetComponent<LineRenderer>();
    }

    void Update()
    {
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 distance = mouse - this.transform.position;

        float mag = distance.magnitude;
        if (mag >= 1)
        {
            transform.Translate(distance.normalized * Time.deltaTime * speed);
        }
        line.SetPosition(0, mouse);
        line.SetPosition(1, this.transform.position);

    }
}
