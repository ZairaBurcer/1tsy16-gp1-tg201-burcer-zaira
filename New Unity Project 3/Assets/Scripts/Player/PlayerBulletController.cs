﻿using UnityEngine;
using System.Collections;

public class PlayerBulletController : MonoBehaviour {

    public GameObject targetEnemy;
    private float speed = 5f;

    void Start()
    {

        Destroy(gameObject, 1);
    }

    void Update()
    {
        GameObject TargetEnemy = Target();
        transform.position = Vector2.MoveTowards(transform.position, TargetEnemy.transform.position, speed * Time.deltaTime);
    }

    GameObject Target()
    {
        GameObject[] enemy = GameObject.FindGameObjectsWithTag("AI");
        GameObject nearbyEnemy = null;
        float temporaryDistance = Mathf.Infinity;

        for (int x = 0; x < enemy.Length; x++)
        {
            Vector2 distance = enemy[x].transform.position - transform.position;
            float currDistance = distance.sqrMagnitude;

            if (currDistance < temporaryDistance)
            {
                nearbyEnemy = enemy[x];
                temporaryDistance = currDistance;
            }

            //foreach (GameObject AI in enemy)
            //{
            //    Vector2 distance = AI.transform.position - transform.position;
            //    float currDistance = distance.sqrMagnitude;

            //    if (currDistance < temporaryDistance)
            //    {
            //        nearbyEnemy = AI;
            //        temporaryDistance = currDistance;
            //    }

            }
            return nearbyEnemy;
    }


  
}
